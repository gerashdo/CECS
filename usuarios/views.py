from email import message

from django.contrib.auth.decorators import permission_required
from preinscripciones.models import Materia, Materia_docente
from django.contrib.auth.models import User
from django.contrib import messages
from django.http.response import HttpResponseRedirect
from django.views.generic.base import TemplateView
from usuarios.models import Aspirante, Docente
from usuarios.forms import AspiranteForm, DocenteForm, DocenteUpdateForm
from django.contrib.auth.views import LoginView
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .models import Aspirante
from django.contrib.auth.models import Group
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login

from django.conf import settings

# Clase para el registro en el sistema de un nuevo aspirante.
class NuevoAspiranteSignUp(CreateView):
    model = Aspirante
    form_class = AspiranteForm
    template_name="signup.html"
    
    # Funcion que valida el formulario con los datos del aspirante.
    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        user_grupo=Group.objects.get(name="Aspirante_Grupo")
        user.groups.add(user_grupo)
        return redirect('usuarios:login')

# Clase que permite ver los datos a detalle de un aspirante.
class DetalleAspirante(DetailView):
    model=Aspirante
    template_name="aspirante/detalle.html"

# Clase que permite eliminar a un aspirante.
class AspiranteEliminar(DeleteView):
    model = Aspirante
    success_url = reverse_lazy('preinscripciones:lista_aspirantes')

# Clase que permite actualizar los datos de un aspirante a travez de un formulario.
class AspiranteActualizar(UpdateView):
    form_class = AspiranteForm
    template_name="aspirante/actualizar.html"
    success_url=reverse_lazy('preinscripciones:lista_aspirantes')
    
# Clase que permite ver una lista de todos los usuarios del sistema.
class UsuarioLista(ListView):
    model=User
    template_name="usuarios/lista.html"
    context_object_name="usuarios"

# Funcion para logear a un usuario dentro del sistema.
def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request.POST)
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request,username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return redirect('preinscripciones:lista_grupos')
            else:
                messages.error(request,"Tu cuenta aun no ha sido validada")
                return redirect("usuarios:login")
    return render(request,"login.html",None)

# Funcion que permite terminar la sesion de un usuario (Administrador o Aspirante).
def logout_view(request):
    logout(request)
    return redirect('usuarios:login')

# Clase que permite logear a usuarios al sistema.
## Permite logear administradores y aspirantes.
## Dependiendo de su perfil, serán las opciones que podrán ver en el sistema.
class LoginUser(LoginView):
    model=User
    template_name="login.html"
    success_url=reverse_lazy("preinscripciones:lista_grupos")

# Clase deprecada.
class LoginView(TemplateView):
  template_name = 'login.html'

  def post(self, request, **kwargs):
    username = request.POST.get('username', False)
    password = request.POST.get('password', False)
    usuario=User.objects.get(username=username)
    user = authenticate(user=usuario, password=password)
    if(not user.is_active):
        messages.error(request,"Datos incorrectos")
        return render(request,self.template_name)
    elif(user is None):
        messages.error(request,"Tu cuenta no ha sido validada aún")
        return render(request, self.template_name)
    else:
        login(request, user)
        return HttpResponseRedirect( settings.LOGIN_REDIRECT_URL )

# ------------------------- DOCENTES ---------------------------------

# Clase que permite agregar docentes por medio de un formulario.
class NuevoDocente(CreateView):
    model = Docente
    form_class = DocenteForm
    template_name="docentes/crear.html"
    success_url = reverse_lazy('docentes:lista_docentes')

# Clase que muestra la lista de los docentes en el sistema.
class DocenteList(ListView):
    model = Docente
    context_object_name="docentes"
    extra_context={
        "grupo_admin":Group.objects.get(name="Administrador_Grupo")
    }
    template_name="docentes/lista.html"

# Clase que permite eliminar un docente del sistema.
class DocenteEliminar(DeleteView):
    model = Docente
    success_url = reverse_lazy('docentes:lista_docentes')

# Clase que permite cambiar los datos de un docente en base a un formulario.
class DocenteActualizar(UpdateView):
    model = Docente
    form_class = DocenteForm
    success_url = reverse_lazy('docentes:lista_docentes')

# Funcion que pemrite ver los datos de un docente en específico.
def ver_docente(request, pk):
    docente=get_object_or_404(Docente,id=pk)
    
    materias_impartidas=Materia_docente.objects.filter(docente=docente).values("materia")
    materias=[]
    for materia in materias_impartidas:
        materia_encontrada=Materia.objects.get(id=materia['materia'])
        materias.append(materia_encontrada)
    if len(materias) != 0:
        docente.materias=materias
    context={
        "docente":docente
    }
    return render(request,"docentes/detalle.html",context)

# Clase que permite ver los detalles de un usuario en especifico.
class DetalleUsuario(DetailView):
    model=User
    template_name="usuarios/detalle.html"
    context_object_name="usuario"

#a
class EditarDocentes(UpdateView):
    model=Docente
    form_class=DocenteUpdateForm
    success_url = reverse_lazy('docentes:lista_docentes')
    template_name="docentes/editar.html"

@permission_required('auth.administrador_permiso', raise_exception=True)
def hacer_admin(request, pk):
    docente= get_object_or_404(Docente, id=pk)
    user_grupo=Group.objects.get(name="Administrador_Grupo")
    docente.groups.add(user_grupo)
    return redirect("docentes:lista_docentes")

@permission_required('auth.administrador_permiso', raise_exception=True)
def quitar_admin(request,pk):
    docente= get_object_or_404(Docente, id=pk)
    user_grupo=Group.objects.get(name="Administrador_Grupo")
    docente.groups.remove(user_grupo)
    return redirect("docentes:lista_docentes")
