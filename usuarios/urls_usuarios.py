from django.urls import path
from . import views

app_name = 'usuarios'

urlpatterns = [
    # LIGAS DE USUARIOS
    path("lista-usuarios/", views.UsuarioLista.as_view(), name="lista_usuarios"),
    path('eliminar/<int:pk>', views.AspiranteEliminar.as_view(), name = 'eliminar'),
    path('actualizar/', views.AspiranteActualizar.as_view(), name = 'actualizar'),
    path('detalle/<int:pk>', views.DetalleAspirante.as_view(), name = 'detalle'),
    path('signup/', views.NuevoAspiranteSignUp.as_view(), name = 'signup_aspirante'),
    path("login/",views.LoginUser.as_view(),name="login"),
    path("logout/",views.logout_view,name="logout"),
]