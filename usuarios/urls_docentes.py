from django.urls import path
from . import views

app_name = 'docentes'

urlpatterns = [
    # LIGAS DE DOCENTES
    path("lista/", views.DocenteList.as_view(), name="lista_docentes"),
    path('eliminar/<int:pk>', views.DocenteEliminar.as_view(), name = 'eliminar_docente'),
    path('actualizar/', views.DocenteActualizar.as_view(), name = 'actualizar_docente'),
    path('nuevo/', views.NuevoDocente.as_view(), name='nuevo_docente'),
    path("ver/<int:pk>",views.ver_docente, name="ver_docente"),
    path('editar/<int:pk>',views.EditarDocentes.as_view(),name="editar_docente"),
    path('admin/<int:pk>',views.hacer_admin,name="hacer_admin"),
    path('quitar-admin/<int:pk>',views.quitar_admin, name="quitar_admin")
]