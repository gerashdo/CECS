from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name='preinscripciones'

urlpatterns = [
    #Aspirantes
    path('lista-aspirantes/', views.lista_aspirantes.as_view(), name='lista_aspirantes'),
    path('lista-aspirantes-rechazados/',views.lista_aspirantes_rechazados,name="lista_aspirantes_rechazados"),
    path('lista-aspirantes-aceptados/',views.lista_aspirantes_aceptados,name="lista_aspirantes_aceptados"),
    path('ver-aspirante/<int:pk>', views.ver_aspirante, name='ver_aspirante'),
    path("aceptar/<int:pk>",views.aceptar_aspirante, name="aceptar_aspirante"),
    path('rechazar/<int:pk>',views.rechazar_aspirante, name="rechazar_aspirante"),
    #Grupos
    path("lista-grupos/", views.ver_listas_grupos.as_view(), name="lista_grupos"),
    path("ver-grupo/<int:pk>",views.ver_grupo.as_view(), name="ver_grupo"),
    path('crear-grupo/', views.crearGrupo.as_view(), name="crear_grupo"),
    path('eliminar-grupo/<int:pk>', views.eliminarGrupo.as_view(), name="eliminar_grupo"),
    path("ver-lista-grupo/<int:pk>",views.ver_lista_alumnos_grupo, name="ver_aspirantes_grupo"),
    path("editar-grupo/<int:pk>",views.editarGrupo.as_view(),name="editar_grupo"),
    path("inscribirme/<int:pk_aspirante>/<int:pk_grupo>",views.inscribir_alumno_grupo, name="inscribir"),
    path("eliminar-aspirante-grupo/<int:pk>",views.eliminar_aspirante_grupo,name="eliminar_aspirante_grupo"),
    #Materias
    path("lista-materias/", views.lista_materias, name="lista_materias"),
    path("nueva-materia/", views.NuevaMateria.as_view(), name="nueva_materia"),
    path("eliminar-materia/<int:pk>",views.eliminar_materia.as_view(), name="eliminar_materia"),
    path("eliminar-materia-docente/<int:id_materia>/<int:id_docente>",views.eliminar_materia_docente, name="eliminar_materia_docente"),
    path("nueva-materia-docente/",views.NuevaMateriaDocente.as_view(),name="nueva_materia_docente"),
    path("ver-materia/<int:id>", views.ver_materia, name="ver_materia"),
    path("agregar-materia-grupo/",views.AgregarGrupoMateria.as_view(), name="agregar_materia_grupo"),
    path("ver-materias-grupo/<int:id>",views.verMateriasGrupo, name="ver_materias_grupo"),
    path("eliminar-grupo-materia-docente/<int:id_grupo>/<int:id_materia_docente>",views.eliminarGrupoMateriaDocente,name="eliminar_grupo_materia_docente"),
    path("editar-materia/<int:pk>",views.EditarMateria.as_view(),name="editar_materia")
]