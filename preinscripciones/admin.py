from .models import Grupo, Materia, Semestre
from django.contrib import admin

# Register your models here.

admin.site.register(Grupo)
admin.site.register(Materia)
admin.site.register(Semestre)