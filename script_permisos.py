import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'CECS.settings')
django.setup()

from django.contrib.auth.models import Permission, Group, User
from django.contrib.contenttypes.models import ContentType

from preinscripciones.models import Semestre
from usuarios.models import Aspirante


Administrador_Grupo = Group.objects.create(name='Administrador_Grupo')
Aspirante_Grupo = Group.objects.create(name='Aspirante_Grupo')

content_type = ContentType.objects.get_for_model(Aspirante)
content_type2 = ContentType.objects.get_for_model(User)


aspirante_permiso = Permission.objects.create(
    codename = 'aspirante_permiso',
    name = 'Permiso requerido para aspirantes',
    content_type = content_type
)
administrador_permiso = Permission.objects.create(
    codename = 'administrador_permiso',
    name = 'Permiso requerido para administradores',
    content_type = content_type2
)



Administrador_Grupo.permissions.add(administrador_permiso)
Administrador_Grupo.permissions.add(aspirante_permiso)
Aspirante_Grupo.permissions.add(aspirante_permiso)

semestre_1 = Semestre(numero = 1)
semestre_2 = Semestre(numero = 2)
semestre_3 = Semestre(numero = 3)
semestre_4 = Semestre(numero = 4)
semestre_5 = Semestre(numero = 5)
semestre_6 = Semestre(numero = 6)
semestre_7 = Semestre(numero = 7)
semestre_8 = Semestre(numero = 8)
semestre_9 = Semestre(numero = 9)
semestre_10 = Semestre(numero = 10)


semestre_1.save()
semestre_2.save()
semestre_3.save()
semestre_4.save()
semestre_5.save()
semestre_6.save()
semestre_7.save()
semestre_8.save()
semestre_9.save()
semestre_10.save()


User.objects.create_superuser('administrador', password='admin_CECS')
